<?php

$json = file_get_contents(__DIR__ . '/input.json');
$json = json_encode(json_decode($json)); // get rid of the pretty printing to produce compact JSON

echo $json . "\n\n"; // shows what the compact JSON looks like

$privateKey = openssl_pkey_get_private("file://" . __DIR__ . "/domain.key", "1234");
$signature = '';
openssl_sign($json, $signature, $privateKey, 'sha256WithRSAEncryption');

echo base64_encode($signature) . "\n"; // shows the signature
